
pragma solidity ^0.4.0;
contract Test {
    //    DECLARA VARIABLE ENTERO SIN SIGNO
    uint suma;
    //  DECLARA CONSTRUCTOR
    function Test(){}
    // GET - SET
    function setSuma(uint valor) public {
     assert(valor > 0);
     suma = suma + valor;}
    function getSuma() public view returns(uint){ return suma; }
}

